jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();


    //Sticky Header
    jQuery(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 200) {
            jQuery(".header").addClass("fixed-header");
        } else {
            jQuery(".header").removeClass("fixed-header");
        }
    });
    
    // Advertiesment Slider
    
    jQuery('.adv-img-sec1').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="la la-angle-left"></i></a>',
      nextArrow: '<a class="slick-next"><i class="la la-angle-right"></i></a>',
    });
    
    // Grid Slider
    
    jQuery('.grid-slider').slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: '<a class="slick-prev"><i class="la la-angle-left"></i></a>',
      nextArrow: '<a class="slick-next"><i class="la la-angle-right"></i></a>',
    });
    
    // Click Toggle 
    
    jQuery(document).ready(function () { 
        jQuery(".toggle-open").click(function () { 
           jQuery("body").toggleClass("sidebar-menu"); 
        }); 
        jQuery(".menu-canvas").click(function () { 
           jQuery("body").removeClass("sidebar-menu"); 
       }); 
    });
});

 
jQuery(document).on('keypress keyup', function(e) { 
    if (e.keyCode == 27) {
        jQuery("body").removeClass("sidebar-menu"); 
    }  
});

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

// Upload Section
jQuery(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

jQuery(document).ready( function() {
    jQuery('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});